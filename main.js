
// #ifndef VUE3
import Vue from 'vue'
import App from './App'
import api from '@/config/http.js'
import util from '@/common/js/utils.js'
import dialog from '@/components/popup/dialog.vue'

Vue.config.productionTip = false

App.mpType = 'app'
Vue.prototype.$api = api;
Vue.prototype.$util = util;

Vue.component('cu-dialog', dialog)

const app = new Vue({
    ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
import App from './App.vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif