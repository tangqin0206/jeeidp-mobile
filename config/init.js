import api from '@/config/http.js'
import util from '@/common/js/utils.js'
// #ifdef APP-PLUS
import APPUpdate from '@/uni_modules/zhouWei-APPUpdate/js_sdk/appUpdate';
// #endif
export default async function() {

	let deviceInfo = uni.getDeviceInfo();

	setTimeout(() => {
		getApp({
			allowDefault: true
		}).globalData.deviceInfo = deviceInfo;
	}, 1)

	// #ifdef APP-PLUS
	APPUpdate();
	// #endif
	
	// #ifdef MP-WEIXIN
	const updateManager = uni.getUpdateManager();
	
	updateManager.onCheckForUpdate(function (res) {
	  // 请求完新版本信息的回调
	  console.log(res.hasUpdate);
	});
	
	updateManager.onUpdateReady(function (res) {
	  uni.showModal({
	    title: '更新提示',
	    content: '新版本已经准备好，是否重启应用？',
	    success(res) {
	      if (res.confirm) {
	        // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
	        updateManager.applyUpdate();
	      }
	    }
	  });
	
	});
	
	updateManager.onUpdateFailed(function (res) {
	  // 新的版本下载失败
	  uni.showModal({
	  	title: '提示',
	  	content: '检查到有新版本，但下载失败，请检查网络设置',
	  	success(res) {
	  		if (res.confirm) {
	  			// 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
	  			updateManager.applyUpdate();
	  		}
	  	}
	  });
	});
	
	uni.login({
		provider: 'weixin',
		scopes: 'auth_base',
		onlyAuthorize: true,
		success: loginRes => {
			let params = {
				code: loginRes.code,
				deviceInfo: JSON.stringify(deviceInfo)
			}
			api.post('/appLogin/loginWechatAuthBase', params).then(res => {
				if(res.data){
					util.saveLoginToken(res.data.token);
				}
				else {
					util.removeLoginToken();
				}
			})
		}
	})
	// #endif

}
