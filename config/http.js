/**
 * 通用uni-app网络请求
 * 基于 Promise 对象实现更简单的 request 使用方式，支持请求和响应拦截
 */
export default {
	config: {
		baseUrl: "http://192.168.0.145:8881/api",
		header: {
			'Content-Type':'application/json;charset=UTF-8'
		},  
		data: {},
		method: "GET",
		dataType: "json",
		responseType: "text",
		success() {},
		fail() {},
		complete() {}
	},
	interceptor: {
		request: null,
		response: null
	},
	request(options) {
		if (!options) {
			options = {}
		}
		options.baseUrl = options.baseUrl || this.config.baseUrl
		options.dataType = options.dataType || this.config.dataType
		options.url = options.baseUrl + options.url
		options.data = options.data || {}
		options.method = options.method || this.config.method
		//TODO 加密数据
		
		//TODO 数据签名
		
		let token = uni.getStorageSync('_jeeidp_token');
		// token = '888888'
		if(token){
			options.header = {
				Authorization: 'Bearer ' + token
			}; 
		}
	   
		return new Promise((resolve, reject) => {
			let _config = null
			
			options.complete = (response) => {
				let statusCode = response.statusCode
				response.config = _config
				if (this.interceptor.response) {
					let newResponse = this.interceptor.response(response)
					if (newResponse) {
						response = newResponse
					}
				}
				// 统一的响应日志记录
				_reslog(response)
				
				//成功
				if (statusCode === 200) { 
					resolve(response.data);
				} else {
					if(statusCode === 401 && response.data && response.data.status === 401){
						uni.removeStorageSync('_jeeidp_token');
						uni.navigateTo({
							url: '/pages/login/login'
						});
					} else {
						
						reject(response.data)
					}
				}
			}

			_config = Object.assign({}, this.config, options)
			_config.requestId = new Date().getTime()

			if (this.interceptor.request) {
				this.interceptor.request(_config)
			}
			
			// 统一的请求日志记录
			_reqlog(_config)
			
			uni.request(_config);
		});
	},
	get(url, data, options) {
		if (!options) {
			options = {}
		}
		options.url = url
		options.data = data
		options.method = 'GET'  
		return this.request(options)
	},
	post(url, data, options) {
		if (!options) {
			options = {}
		}
		options.url = url
		options.data = data
		options.method = 'POST'
		return this.request(options)
	},
	upload(filePath, category) {
		
		let options = {
			url: this.config.baseUrl + '/common/uploadFile',
			filePath: filePath,
			name: 'file',
			formData: {
				'category': category
			}
		};
		
		let token = uni.getStorageSync('_jeeidp_token');
		// token = '888888'
		if(token){
			options.header = {
				Authorization: 'Bearer ' + token
			}; 
		}
		
		return new Promise((resolve, reject) => {
			
			options.complete = (response) => {
				let statusCode = response.statusCode
				let data = JSON.parse(response.data)
				//成功
				if (statusCode === 200) { 
					
					resolve(data);
				} else {
					if(statusCode === 401 && response.data && data.status === 401){
						uni.removeStorageSync('_jeeidp_token');
						uni.navigateTo({
							url: '/pages/login/login'
						});
					} else {
						
						reject(data)
					}
				}
			}
			
			uni.uploadFile(options)
		})
	}
}


/**
 * 请求接口日志记录
 */
function _reqlog(req) {
	if (process.env.NODE_ENV === 'development') {
		console.log("【" + req.requestId + "】 地址：" + req.url)
		if (req.data) {
			console.log("【" + req.requestId + "】 请求参数：" + JSON.stringify(req.data))
		}
	}
}

/**
 * 响应接口日志记录
 */
function _reslog(res) {
	let _statusCode = res.statusCode;
	if (process.env.NODE_ENV === 'development') {
		
		// console.log("【" + res.config.requestId + "】 地址：" + res.config.url)
		// if (res.config.data) {
		// 	console.log("【" + res.config.requestId + "】 请求参数：" + JSON.stringify(res.config.data))
		// }
		if(res.data){
			console.log("【" + res.config.requestId + "】 响应结果：" + JSON.stringify(res.data))
			console.log(res.data);
		}
	}
}

