import api from '@/config/http.js'
export default {
	showImage(url){
		if(!url){
			return;
		}
		if(url && url.indexOf('https://thirdwx.qlogo.cn')>-1){ //微信头像
			return url
		}
		if(url && url.indexOf('http://tmp/')>-1){ //本地图片
			return url
		}
		if(url && url.indexOf('wxfile://')>-1){ //本地图片
			return url
		}
		if(url && url.indexOf('_doc/')>-1){ //本地图片
			return url
		}
		if(url && url.indexOf('http') == 0){
			
			return url;
		}
		return api.config.baseUrl + '/common/showImage' + url;
	},
	showVideo(url){
		return api.config.baseUrl + '/common/downloadFile' + url;
	},
	checkLogin(){
		let token = uni.getStorageSync('_jeeidp_token');
		if(token){
			return true;
		}
		else {
			return false;
		}
	},
	getLoginToken(){
		return uni.getStorageSync('_jeeidp_token');
	},
	saveLoginToken(token){
		uni.setStorageSync('_jeeidp_token', token);
	},
	removeLoginToken(){
		uni.removeStorageSync('_jeeidp_token')
	},
	messageTip(message){
		uni.showToast({
			icon: 'none',
			duration: 2000,
			title: message
		})
	}
}